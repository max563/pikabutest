package ru.nn.serg.pikabutest.utility;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import ru.nn.serg.pikabutest.dao.Story;

public class MDiffUtilCallback extends DiffUtil.ItemCallback<Story> {

    @Override
    public boolean areItemsTheSame(@NonNull Story oldItem, @NonNull Story newItem) {
        return oldItem.getId()==newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull Story oldItem, @NonNull Story newItem) {
        return oldItem.getId()==newItem.getId()
                &&oldItem.getTitle().equals(newItem.getTitle())
                && oldItem.getBody().equals(newItem.getBody());
    }
}
