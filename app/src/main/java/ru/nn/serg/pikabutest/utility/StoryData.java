package ru.nn.serg.pikabutest.utility;

import java.util.List;

import ru.nn.serg.pikabutest.dao.Story;

public class StoryData {
    private List<Story> data;
    private int position;

    public StoryData(List<Story> data, int position) {
        this.data = data;
        this.position = position;
    }

    public List<Story> getData() {
        return data;
    }

    public void setData(List<Story> data) {
        this.data = data;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
