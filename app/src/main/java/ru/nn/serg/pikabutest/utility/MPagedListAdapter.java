package ru.nn.serg.pikabutest.utility;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.ArrayList;
import java.util.List;

import ru.nn.serg.pikabutest.R;
import ru.nn.serg.pikabutest.dao.Story;
import ru.nn.serg.pikabutest.ui.OneStoryActivity;

public class MPagedListAdapter extends PagedListAdapter<Story, MPagedListAdapter.ItemStoryHolder> {

    public MPagedListAdapter(@NonNull DiffUtil.ItemCallback<Story> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public ItemStoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_layout,parent,false);
        return new ItemStoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemStoryHolder holder, int position) {
        holder.bind(getItem(position));
    }

    //Снимаем слушателя со свитч бокса в момент переиспользования холдера, иначе со свитч боксом
    //происходят хаотичные переключения при переиспользовании холдера, слушатель потом ставиться в биндере
    @Override
    public void onViewRecycled(@NonNull ItemStoryHolder holder) {
        if(holder.switchMaterial!=null){
            holder.switchMaterial.setOnCheckedChangeListener(null);
        }
        super.onViewRecycled(holder);
    }

    class ItemStoryHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewBody;
        ImageView imageView,imageView2;
        View view;
        //List<ImageView> imageViews;
        LinearLayout linearLayoutChild;
        SwitchMaterial switchMaterial;

        SharedPreferences preferences;

        public ItemStoryHolder(@NonNull View itemView) {
            super(itemView);
            view=itemView;
            textViewTitle = (TextView) itemView.findViewById(R.id.title);
            textViewBody = (TextView) itemView.findViewById(R.id.body);
            imageView=(ImageView) itemView.findViewById(R.id.imageView);
            imageView2=(ImageView) itemView.findViewById(R.id.imageView2);

            linearLayoutChild = (LinearLayout) itemView.findViewById(R.id.linearLayoutChild);
            switchMaterial=(SwitchMaterial) itemView.findViewById(R.id.save_switch);

            preferences=view.getContext().getSharedPreferences ("prefs", Context.MODE_PRIVATE);
        }

        void bind(Story story){
            Log.d("","Пост:"+story.getTitle()+" ID="+story.getId());
            textViewTitle.setText(story.getTitle());
            textViewBody.setText(story.getBody());


            switchMaterial=(SwitchMaterial) itemView.findViewById(R.id.save_switch);

            boolean isSaved=preferences.getBoolean(String.valueOf( story.getId()),false);
            switchMaterial.setChecked(isSaved);
            story.setSaved(isSaved);
            StaticFunctions.updateSwitch(switchMaterial);
            /*
            if(switchMaterial.isChecked()){
                switchMaterial.setText(R.string.Saved);
            }else {
                switchMaterial.setText(R.string.Save);
            }

             */




            /*
            if (linearLayoutChild.getChildCount()>2){
                for (int i=2;i<linearLayoutChild.getChildCount();i++){
                    linearLayoutChild.removeViewAt(i);
                }
            }

             */

            //добавляем картинку(и) если есть
            int numberOfPictures = 0;
            if(story.getImages()!=null){
                numberOfPictures = story.getImages().length;
            }
            Log.d("","Колличество картинок:"+numberOfPictures);


            if (numberOfPictures>0){
                List<ImageView> imageViews=new ArrayList<>();
                imageViews.add(imageView);
                imageViews.add(imageView2);
                for(int i=0;i<numberOfPictures;i++){
                    Log.d("","Post pictures:"+story.getImages()[i]);
                    GlideApp.with(view.getContext())
                            .load(story.getImages()[i])
                            //.placeholder(new ColorDrawable(Color.RED))
                            .into(imageViews.get(i));
                }
            }else {
                GlideApp.with(view.getContext())
                        .clear(imageView);
                GlideApp.with(view.getContext())
                        .clear(imageView2);
            }

            /*
            if (numberOfPictures>0){
                List<ImageView> imageViews=new ArrayList<>();
                ViewGroup.LayoutParams imageViewLayoutParams =
                        new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                                , ViewGroup.LayoutParams.WRAP_CONTENT);
                for (int i=0; i<numberOfPictures; i++){
                    Log.d("","Post pictures:"+story.getImages()[i]);
                    imageViews.add(new ImageView(linearLayoutChild.getContext()));
                    GlideApp.with(view.getContext())
                            .load(story.getImages()[i])
                            .into(imageViews.get(i));
                    imageViews.get(i).setLayoutParams(imageViewLayoutParams);
                    linearLayoutChild.addView(imageViews.get(i));
                }
            }

             */

            textViewTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(view.getContext(), OneStoryActivity.class);
                    intent.putExtra("id",story.getId());
                    intent.putExtra("title",story.getTitle());
                    intent.putExtra("body",story.getBody());
                    intent.putExtra("images",story.getImages());
                    intent.putExtra("saved", story.isSaved());
                    view.getContext().startActivity(intent);
                }
            });

            switchMaterial.setOnCheckedChangeListener (new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.d("","On checked. Story id:"+story.getId()+" isChecked="+isChecked+" title:"+story.getTitle());
                    story.setSaved(isChecked);
                    if(isChecked){
                        SharedPreferences.Editor ed = preferences.edit();
                        ed.putBoolean(String.valueOf(story.getId()), true);
                        ed.apply();
                    }else {
                        SharedPreferences.Editor ed = preferences.edit();
                        ed.remove(String.valueOf(story.getId()));
                        ed.apply();
                    }


                    StaticFunctions.updateSwitch(switchMaterial);
                }
            });


        }
    }
}
