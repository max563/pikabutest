package ru.nn.serg.pikabutest.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.nn.serg.pikabutest.dao.JSONPlaceHolderApi;

/**
 * Класс является сетевым сервисом для сетевого взаимодействия.
 * синглтон
 */
public class NetworkService {

    private static NetworkService mInstance;
    private static final String BASE_URL="https://pikabu.ru/";
    private Retrofit mRetrofit;

    // ********Реализуем singleton паттерн*********
    private NetworkService(){
        mRetrofit=new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    };
    public static NetworkService getInstance(){
        if (mInstance == null) {
            mInstance = new NetworkService();
        }
        return mInstance;
    }
    //*********************************************

    public JSONPlaceHolderApi getJSONApi(){
        return mRetrofit.create(JSONPlaceHolderApi.class);
    }
}
