package ru.nn.serg.pikabutest.utility;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.paging.PositionalDataSource;

import java.util.List;

import ru.nn.serg.pikabutest.dao.Story;

public class MDataSource extends PositionalDataSource<Story> {

    private final  StoryStorage storyStorage;

    public MDataSource(StoryStorage storyStorage){
        this.storyStorage=storyStorage;
    }
    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback<Story> callback) {
        Log.d("", "loadInitial, requestedStartPosition = " + params.requestedStartPosition +
                ", requestedLoadSize = " + params.requestedLoadSize);
        StoryData result = storyStorage.getInitialData(params.requestedStartPosition, params.requestedLoadSize);
        callback.onResult(result.getData(),result.getPosition());
    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<Story> callback) {
        Log.d("", "loadRange, startPosition = " + params.startPosition + ", loadSize = " + params.loadSize);
        List<Story> result = storyStorage.getData(params.startPosition, params.loadSize);
        callback.onResult(result);
    }
}
