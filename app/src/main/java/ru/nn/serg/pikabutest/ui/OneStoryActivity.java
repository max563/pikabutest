package ru.nn.serg.pikabutest.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.ArrayList;
import java.util.List;

import ru.nn.serg.pikabutest.R;
import ru.nn.serg.pikabutest.utility.GlideApp;
import ru.nn.serg.pikabutest.utility.StaticFunctions;

public class OneStoryActivity extends AppCompatActivity {

    SharedPreferences preferences;
    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_story);

        LinearLayout ll = findViewById(R.id.ll_activity_one_story);

        LayoutInflater inflater=getLayoutInflater();
        View view = inflater.inflate(R.layout.post_layout,ll,false);

        preferences=getSharedPreferences("prefs", MODE_PRIVATE);

        Intent intent=getIntent();
        TextView title = view.findViewById(R.id.title);
        TextView body = view.findViewById(R.id.body);
        ImageView imageView = view.findViewById(R.id.imageView);
        ImageView imageView2 = view.findViewById(R.id.imageView2);
        SwitchMaterial switchMaterial = view.findViewById(R.id.save_switch);

        id=intent.getIntExtra("id",0);
        title.setText(intent.getStringExtra("title"));
        body.setText(intent.getStringExtra("body"));

        switchMaterial.setChecked(intent.getBooleanExtra("saved",false));
        StaticFunctions.updateSwitch(switchMaterial);

        String[] images = intent.getStringArrayExtra("images");

        //добавляем картинку(и) если есть
        int numberOfPictures = 0;
        if(images!=null){
            numberOfPictures = images.length;
        }
        Log.d("","Колличество картинок:"+numberOfPictures);


        if (numberOfPictures>0){
            Log.d("","1111111");
            List<ImageView> imageViews=new ArrayList<>();
            imageViews.add(imageView);
            imageViews.add(imageView2);
            for(int i=0;i<numberOfPictures;i++){
                Log.d("","Post pictures:"+images[i]);
                GlideApp.with(this)
                        .load(images[i])
                        //.placeholder(new ColorDrawable(Color.RED))
                        .into(imageViews.get(i));
            }
        }else {
            GlideApp.with(this)
                    .clear(imageView);
            GlideApp.with(this)
                    .clear(imageView2);
        }

        ll.addView(view);

        switchMaterial.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    SharedPreferences.Editor ed = preferences.edit();
                    ed.putBoolean(String.valueOf(id), true);
                    ed.apply();
                }else {
                    SharedPreferences.Editor ed = preferences.edit();
                    ed.remove(String.valueOf(id));
                    ed.apply();
                }
                StaticFunctions.updateSwitch(switchMaterial);
            }
        });
    }
}