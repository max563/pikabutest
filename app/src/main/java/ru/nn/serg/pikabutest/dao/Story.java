package ru.nn.serg.pikabutest.dao;

import com.google.gson.annotations.Expose;

public class Story{
    @Expose
    private int id;
    @Expose
    private String title;
    @Expose
    private String body;
    @Expose
    private String[] images;

    private boolean isSaved = false;

    public Story(int id, String title, String body, String[] images) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }
}
