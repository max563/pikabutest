package ru.nn.serg.pikabutest.dao;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JSONPlaceHolderApi {
    //Тут ориентируемся чтобы получился такой запрос:
    // https://pikabu.ru/page/interview/mobile-app/test-api/story.php?id=***
    // https://pikabu.ru - подставляется в классе NetworkService
    // Один пост
    @GET("page/interview/mobile-app/test-api/story.php")
    public Call<Story> getStoryWithID(@Query("id") int id);

    //Все посты
    @GET("page/interview/mobile-app/test-api/feed.php")
    public Call<List<Story>> getAllStories();
}
