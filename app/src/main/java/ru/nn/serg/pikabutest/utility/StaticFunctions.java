package ru.nn.serg.pikabutest.utility;

import com.google.android.material.switchmaterial.SwitchMaterial;

import ru.nn.serg.pikabutest.R;

public class StaticFunctions {

    public static void updateSwitch(SwitchMaterial switchMaterial){
        if(switchMaterial.isChecked()){
            switchMaterial.setText(R.string.Saved);
        }else {
            switchMaterial.setText(R.string.Save);
        }
    }
}
