package ru.nn.serg.pikabutest.ui.PostFeed;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PostFeedViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public PostFeedViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is post feed fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}