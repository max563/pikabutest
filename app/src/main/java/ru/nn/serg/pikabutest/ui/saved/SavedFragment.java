package ru.nn.serg.pikabutest.ui.saved;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import ru.nn.serg.pikabutest.R;
import ru.nn.serg.pikabutest.dao.Story;
import ru.nn.serg.pikabutest.utility.MDiffUtilCallback;
import ru.nn.serg.pikabutest.utility.MPagedListAdapter;
import ru.nn.serg.pikabutest.utility.MSourceFactory;
import ru.nn.serg.pikabutest.utility.StoryStorage;

public class SavedFragment extends Fragment {

    private SavedViewModel savedViewModel;

    RecyclerView recyclerView;

    //Data Source
    MSourceFactory sourceFactory;
    //Paged List
    PagedList.Config config;
    LiveData<PagedList<Story>> pagedListLiveData;
    //Adapter
    private MPagedListAdapter adapter;

    TextView textView;
    SharedPreferences preferences;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        savedViewModel =
                ViewModelProviders.of(this).get(SavedViewModel.class);
        View root = inflater.inflate(R.layout.fragment_saved, container, false);
        recyclerView = root.findViewById(R.id.recycler_view_saved);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        textView = root.findViewById(R.id.text_saved);
        savedViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        //Data Source
        sourceFactory=new MSourceFactory(new StoryStorage(getContext(),StoryStorage.DATA_FROM_ID));

        //Paged List
        config=new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(5)
                .build();

        pagedListLiveData = new LivePagedListBuilder<>(sourceFactory, config)
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .build();

        //Adapter
        adapter = new MPagedListAdapter(new MDiffUtilCallback());

        preferences=getContext().getSharedPreferences ("prefs", Context.MODE_PRIVATE);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("", "Saved fragment - On Activity created");


        pagedListLiveData.observe(getViewLifecycleOwner(), new Observer<PagedList<Story>>() {
            @Override
            public void onChanged(PagedList<Story> stories) {
                //Log.d("", "submit PagedList");
                adapter.submitList(stories);
            }
        });

        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("", "Saved fragment - On Resume");
        adapter.notifyDataSetChanged();


        if (getItemCountFromPreferences()>0){
            recyclerView.setVisibility(RecyclerView.VISIBLE);
            textView.setVisibility(TextView.INVISIBLE);
        }else {
            recyclerView.setVisibility(RecyclerView.INVISIBLE);
            textView.setVisibility(TextView.VISIBLE);
        }
    }

    private int getItemCountFromPreferences(){
        Map<String, ?> mapFromPrefs = preferences.getAll();
        List<Integer> idsFromPrefs = new ArrayList<>();
        for (Map.Entry<String, ?> entry : mapFromPrefs.entrySet()) {
            //Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
            idsFromPrefs.add(Integer.parseInt(entry.getKey()));
        }
        return idsFromPrefs.size();
    }
}