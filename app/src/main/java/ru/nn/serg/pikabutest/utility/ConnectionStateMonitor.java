package ru.nn.serg.pikabutest.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

/**
 * Сообщает о доступности сети средствами LiveData
 */
public class ConnectionStateMonitor extends ConnectivityManager.NetworkCallback {

    final NetworkRequest networkRequest;

    private static ConnectionStateMonitor instance;

    public static ConnectionStateMonitor getInstance(){
        if (instance!=null){
            return instance;
        }
        instance = new ConnectionStateMonitor();
        return instance;
    }

    private ConnectionStateMonitor() {
        networkRequest = new NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI).build();
    }

    public void enable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        connectivityManager.registerNetworkCallback(networkRequest , this);
    }

    // Likewise, you can have a disable method that simply calls ConnectivityManager.unregisterNetworkCallback(NetworkCallback) too.

    @Override
    public void onAvailable(Network network) {
        Log.d("","Connection state monitor:onAvailable");
        //isNetworkConnection = true;
        liveData.postValue(true);
    }

    @Override
    public void onLost(Network network) {
        Log.d("","Connection state monitor:onLost");
        //isNetworkConnection = false;
        liveData.postValue(false);
    }

    private MutableLiveData<Boolean> liveData = new MutableLiveData<>();

    public LiveData<Boolean> getData() {
        return liveData;
    }
}