package ru.nn.serg.pikabutest.ui.PostFeed;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.concurrent.Executors;

import ru.nn.serg.pikabutest.R;
import ru.nn.serg.pikabutest.dao.Story;
import ru.nn.serg.pikabutest.utility.ConnectionStateMonitor;
import ru.nn.serg.pikabutest.utility.MDiffUtilCallback;
import ru.nn.serg.pikabutest.utility.MPagedListAdapter;
import ru.nn.serg.pikabutest.utility.MSourceFactory;
import ru.nn.serg.pikabutest.utility.StoryStorage;

public class PostFeedFragment extends Fragment {

    RecyclerView recyclerView;

    //Data Source
    MSourceFactory sourceFactory;
    //Paged List
    PagedList.Config config;
    LiveData<PagedList<Story>> pagedListLiveData;

    ConnectionStateMonitor connectionStateMonitor;
    LiveData<Boolean> ldConnectionStateMonitor;
    //Adapter
    private MPagedListAdapter adapter;

    private TextView textView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        PostFeedViewModel postFeedViewModel = ViewModelProviders.of(this).get(PostFeedViewModel.class);
        View root = inflater.inflate(R.layout.fragment_post_feed, container, false);
        textView = root.findViewById(R.id.text_post_feed);
        recyclerView = root.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        postFeedViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        //Data Source
        sourceFactory = new MSourceFactory(new StoryStorage(getContext(), StoryStorage.DATA_ALL));

        //Paged List
        config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(10)
                .build();

        pagedListLiveData = new LivePagedListBuilder<>(sourceFactory, config)
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .build();

        //Adapter
        adapter = new MPagedListAdapter(new MDiffUtilCallback());

        //устанавливаем слушателя на состояние сети
        connectionStateMonitor = ConnectionStateMonitor.getInstance();
        connectionStateMonitor.enable(getContext());
        ldConnectionStateMonitor = connectionStateMonitor.getData();
        ldConnectionStateMonitor.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                Log.d("", "live data is here:"+aBoolean);
                //если нет сети - уведомляем, либо снимаем уведомление
                setNetworkPresent(aBoolean);
            }
        });

        return root;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("", "My fragment - On Activity created");


        pagedListLiveData.observe(getViewLifecycleOwner(), new Observer<PagedList<Story>>() {
            @Override
            public void onChanged(PagedList<Story> stories) {
                Log.d("", "submit PagedList");
                adapter.submitList(stories);
            }
        });

        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("", "My fragment - On Resume");
        //adapter.notifyDataSetChanged();
        //setNetworkPresent(Variables.isNetworkConnected);

    }

    private void setNetworkPresent(final boolean isPresent){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isPresent) {
                    recyclerView.setVisibility(RecyclerView.VISIBLE);
                    textView.setVisibility(TextView.INVISIBLE);
                    adapter.notifyDataSetChanged();
                    Log.d("", "Network:true");
                } else {
                    recyclerView.setVisibility(RecyclerView.INVISIBLE);
                    textView.setVisibility(TextView.VISIBLE);
                    Log.d("", "Network:false");
                }
            }
        });
    }

}