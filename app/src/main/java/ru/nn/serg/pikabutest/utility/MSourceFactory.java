package ru.nn.serg.pikabutest.utility;


import androidx.paging.DataSource;

import ru.nn.serg.pikabutest.dao.Story;

public class MSourceFactory extends DataSource.Factory<Integer, Story> {

    private final StoryStorage storyStorage;

    public MSourceFactory(StoryStorage storyStorage) {
        this.storyStorage = storyStorage;
    }

    @Override
    public DataSource create() {
        return new MDataSource(storyStorage);
    }
}
