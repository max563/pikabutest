package ru.nn.serg.pikabutest.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.nn.serg.pikabutest.dao.JSONPlaceHolderApi;
import ru.nn.serg.pikabutest.dao.Story;
import ru.nn.serg.pikabutest.service.NetworkService;

public class StoryStorage {

    public static final int DATA_ALL = 0;
    public static final int DATA_FROM_ID = 1;

    private List<Integer> ids = new ArrayList<>();

    Context context;
    //Режим работы
    private int operatingMode;

    SharedPreferences preferences;

    private List<Story> stories;
    JSONPlaceHolderApi api = NetworkService.getInstance().getJSONApi();

    public StoryStorage(Context context, int operatingMode) {
        this.context=context;
        this.operatingMode = operatingMode;
        preferences= context.getSharedPreferences ("prefs",Context.MODE_PRIVATE);
    }

    public List<Story> getData(int requestedStartPosition, int requestedLoadSize){
        int endPosition = requestedStartPosition+requestedLoadSize;

        Log.d("", "StoryStorage->getData requestedStartPosition"+requestedStartPosition+
                " requestedLoadSize"+requestedLoadSize);

        //берем дату в завимистости от режима работы
        stories = getStories(operatingMode, requestedStartPosition,requestedLoadSize);

        if (endPosition>stories.size()){
            return stories.subList(requestedStartPosition, stories.size());
        }
        return stories.subList(requestedStartPosition, endPosition);
    }



    public StoryData getInitialData(int requestedStartPosition, int requestedLoadSize) {
        StoryData storyData = new StoryData(null,0);
        int endPosition = requestedStartPosition+requestedLoadSize;

        stories = getStories(operatingMode, requestedStartPosition,requestedLoadSize);

        //При холодном запуске иногда возникают excepton из за того что stories не успел инициализироваться
        //по этому делаем костыль
        if(stories==null) {
            stories = new ArrayList<Story>();
        }

        if (endPosition>stories.size()){
            int startpos = stories.size()-requestedLoadSize<0?0:stories.size()-requestedLoadSize;
            storyData.setData(stories.subList(startpos,stories.size()));
            storyData.setPosition(startpos);
            return storyData;
        }

        storyData.setData(stories.subList(requestedStartPosition, endPosition));
        storyData.setPosition(requestedStartPosition);
        return storyData;

    }

    private List<Story> getStories(int operatingMode, int requestedStartPosition, int requestedLoadSize) {

        int endPosition = requestedStartPosition+requestedLoadSize;

        switch (operatingMode){
            case DATA_ALL:
                //Здесь в связи с тестовостью примера, не реализовано получения части данных,
                //получаем сразу все данные
                stories = getAllStories();
                break;
            case DATA_FROM_ID:
                ids.clear();
                Map<String, ?> mapFromPrefs = preferences.getAll();
                List<Integer> idsFromPrefs = new ArrayList<>();
                for (Map.Entry<String, ?> entry : mapFromPrefs.entrySet()) {
                    idsFromPrefs.add(Integer.parseInt(entry.getKey()));
                }
                for (int i=requestedStartPosition;i<endPosition;i++){
                    if((i-requestedStartPosition)<idsFromPrefs.size()) {
                        ids.add(idsFromPrefs.get(i-requestedStartPosition));
                    }else {
                        break;
                    }
                }
                stories= getStoriesFromID(ids);
                break;
        }
        return stories;
    }

    private List<Story> getAllStories(){
        try {
            //получение данных с блокировкой потока, этот метод вызывается не с UI потока
            stories = api.getAllStories().execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stories;
    }

    private List<Story> getStoriesFromID(List<Integer> ids){

        List<Story> stories = new ArrayList<>();
        try {
            for (int id:ids){
                stories.add(api.getStoryWithID(id).execute().body());
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return stories;
    }

}
