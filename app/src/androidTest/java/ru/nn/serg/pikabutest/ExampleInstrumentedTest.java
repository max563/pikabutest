package ru.nn.serg.pikabutest;

import android.content.Context;
import android.util.Log;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import ru.nn.serg.pikabutest.dao.JSONPlaceHolderApi;
import ru.nn.serg.pikabutest.dao.Story;
import ru.nn.serg.pikabutest.service.NetworkService;
import ru.nn.serg.pikabutest.utility.StoryStorage;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("ru.nn.serg.pikabutest", appContext.getPackageName());
    }

    @Test
    public void storyStorageTestAllData(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        StoryStorage storyStorage = new StoryStorage(appContext,StoryStorage.DATA_ALL);
        List<Story> stories = storyStorage.getData(0,9);
        assertEquals(stories.get(0).getId(), 1);
    }

    @Test
    public void apiTest() throws IOException {
        JSONPlaceHolderApi api = NetworkService.getInstance().getJSONApi();
        List<Story> stories;
        stories = api.getAllStories().execute().body();
        //Log.d("Mymsg:", String.valueOf(stories));
        assertEquals(stories.get(0).getId(), 1);
    }
}